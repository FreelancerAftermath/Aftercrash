#pragma once
#include <iostream>
#include <fstream>
#include <stdexcept>

class Logger
{
public:
	// Logger cannot exist without file.
	Logger() = delete;

	// Disable copy constructor  since std::ofstream is not copyable.
	Logger(Logger const&) = delete;

	// Constructor
	explicit Logger(std::string const& f_path)
	{
		log_file.open(f_path, std::ofstream::in | std::ofstream::out | std::ofstream::app);
		if (!log_file.is_open())
		{
			throw std::runtime_error("Unable to open log file");
		}
	} 

	// Disable copy.
	Logger& operator=(Logger const&) = delete;

	// Cleanup.
	~Logger()
	{
		log_file.close();
	}

	// Write a single value into log file stream.
	template<typename T>
	void write(T const& v)
	{
		const std::string timestamp = GenerateTimestamp();
		log_file << timestamp << " " << v;
		log_file << "\n";
		log_file.flush();
	}

	// Write multiple values.
	template<typename Arg, typename ...Args>
	void write(Arg const& arg, Args const&... args)
	{
		// here we write the first value of the values list.
		write(arg);
		// here we recursively pass the rest values to the function.
		write(args...);
		log_file.flush();
	}
private:
	// Log file stream.
	std::ofstream log_file;
};