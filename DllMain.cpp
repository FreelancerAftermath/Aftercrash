﻿#include <csignal>
#include <Windows.h>
#include <tchar.h>
#include <cstdio>
#include <filesystem>
#include <Psapi.h>

#include "Main.h"
#include "StackWalker.h"
#include "Logger.h"
#include "toml.hpp"
#include "Curl/curl.h"

static Logger* logger = nullptr;

#pragma comment(lib, "libcurl.lib")

unsigned char *thornLoadData;
typedef void *(__cdecl *ScriptLoadPtr)(const char*);
ScriptLoadPtr _ThornScriptLoad;

namespace fs = std::filesystem;

#define StrCpy(x, y, z) \
	if ((y) <= 0) \
		return; \
	strncpy_s(x, y, z, _TRUNCATE); \
	(x)[(y) - 1] = 0;

std::string GenerateTimestamp()
{
	std::time_t rawtime;
	char timestamp[100];
	std::time(&rawtime);
	std::tm* timeinfo = std::localtime(&rawtime);
	std::strftime(timestamp, 80, "%Y-%m-%d / %H:%M:%S", timeinfo);
	return std::string("[") + timestamp + "]";
}

void Detour(unsigned char* pOFunc, void* pHkFunc, unsigned char* originalData)
{
	DWORD dwOldProtection = 0; // Create a DWORD for VirtualProtect calls to allow us to write.
	BYTE bPatch[5]; // We need to change 5 bytes and I'm going to use memcpy so this is the simplest way.
	bPatch[0] = 0xE9; // Set the first byte of the byte array to the op code for the JMP instruction.
	VirtualProtect((void*)pOFunc, 5, PAGE_EXECUTE_READWRITE, &dwOldProtection); // Allow us to write to the memory we need to change
	DWORD dwRelativeAddress = (DWORD)pHkFunc - (DWORD)pOFunc - 5; // Calculate the relative JMP address.
	memcpy(&bPatch[1], &dwRelativeAddress, 4); // Copy the relative address to the byte array.
	memcpy(originalData, pOFunc, 5);
	memcpy(pOFunc, bPatch, 5); // Change the first 5 bytes to the JMP instruction.
	VirtualProtect((void*)pOFunc, 5, dwOldProtection, nullptr); // Set the protection back to what it was.
}

void UnDetour(unsigned char* pOFunc, unsigned char* originalData)
{
	DWORD dwOldProtection = 0; // Create a DWORD for VirtualProtect calls to allow us to write.
	VirtualProtect((void*)pOFunc, 5, PAGE_EXECUTE_READWRITE, &dwOldProtection); // Allow us to write to the memory we need to change
	memcpy(pOFunc, originalData, 5);
	VirtualProtect((void*)pOFunc, 5, dwOldProtection, nullptr); // Set the protection back to what it was.
}

class AftWalker : public StackWalker
{
public:
	std::ostringstream ss;
protected:	
	void OnCallstackEntry(CallstackEntryType eType, CallstackEntry& entry) override
	{
		CHAR   buffer[STACKWALK_MAX_NAMELEN];
		size_t maxLen = STACKWALK_MAX_NAMELEN;
		
		#if _MSC_VER >= 1400
		maxLen = _TRUNCATE;
		#endif
		
		if (eType != lastEntry && entry.offset != 0)
		{
			if (entry.name[0] == 0)
				StrCpy(entry.name, STACKWALK_MAX_NAMELEN, "(function-name not available)");
			
			if (entry.undName[0] != 0)
				StrCpy(entry.name, STACKWALK_MAX_NAMELEN, entry.undName);
			
			if (entry.undFullName[0] != 0)
				StrCpy(entry.name, STACKWALK_MAX_NAMELEN, entry.undFullName);
			
			if (entry.lineFileName[0] == 0)
			{
				StrCpy(entry.lineFileName, STACKWALK_MAX_NAMELEN, "(filename not available)");
				
				if (entry.moduleName[0] == 0)
					StrCpy(entry.moduleName, STACKWALK_MAX_NAMELEN, "(module-name not available)");
				
				_snprintf_s(buffer, maxLen, "%p (%s): %s: %s\n", LPVOID(entry.offset), entry.moduleName,
					entry.lineFileName, entry.name);
			}
			else
				_snprintf_s(buffer, maxLen, "%s (%d): %s\n", entry.lineFileName, entry.lineNumber,
					entry.name);

			// Store current callstack inside
			this->ss << buffer << std::endl;
			buffer[STACKWALK_MAX_NAMELEN - 1] = 0; // null terminate buffer
			OnOutput(buffer); // output 
		}
	}
	
	void OnLoadModule(LPCSTR img, LPCSTR mod, DWORD64 baseAddr, DWORD size, DWORD result, LPCSTR symType, LPCSTR pdbName, ULONGLONG fileVersion) override {};	
	void OnOutput(LPCSTR szText) override { logger->write(szText); }
};

static BOOL PreventSetUnhandledExceptionFilter()
{
	HMODULE hKernel32 = LoadLibrary("kernel32.dll");
	if (hKernel32 == nullptr)
		return 0;
	
	void* pOrgEntry = GetProcAddress(hKernel32, "SetUnhandledExceptionFilter");
	if (pOrgEntry == nullptr)
		return 0;

	// Code for x86:
	// 33 C0                xor         eax,eax
	// C2 04 00             ret         4
	unsigned char szExecute[] = { 0x33, 0xC0, 0xC2, 0x04, 0x00 };

	DWORD dwOldProtect = 0;
	BOOL  bProt = VirtualProtect(pOrgEntry, sizeof szExecute, PAGE_EXECUTE_READWRITE, &dwOldProtect);

	SIZE_T bytesWritten = 0;
	BOOL   bRet = WriteProcessMemory(GetCurrentProcess(), pOrgEntry, szExecute, sizeof szExecute,
		&bytesWritten);

	if (bProt != false && dwOldProtect != PAGE_EXECUTE_READWRITE)
	{
		DWORD dwBuf;
		VirtualProtect(pOrgEntry, sizeof szExecute, dwOldProtect, &dwBuf);
	}
	return bRet;
}

static char s_szExceptionLogFileName[_MAX_PATH] = "\\exceptions.log"; // default
static BOOL  s_bUnhandledExeptionFilterSet = false;
static long __stdcall CrashHandlerExceptionFilter(EXCEPTION_POINTERS* pExPtrs)
{
	if (pExPtrs->ExceptionRecord->ExceptionCode == EXCEPTION_STACK_OVERFLOW)
	{
		static char MyStack[1024 * 128]; // be sure that we have enough space...
		// it assumes that DS and SS are the same!!! (this is the case for Win32)
		// change the stack only if the selectors are the same (this is the case for Win32)
		//__asm push offset MyStack[1024*128];
		//__asm pop esp;
		__asm mov eax, offset MyStack[1024 * 128];
		__asm mov esp, eax;
	}

	AftWalker sw;
	sw.ShowCallstack(GetCurrentThread(), pExPtrs->ContextRecord);
	std::stringstream str;
	LPCSTR caption = "Fatal Crash. Unknown Error Code";

	const char* p = nullptr;
	if (addressErrorCodes.find(DWORD(GetModuleHandle(nullptr)) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)) != addressErrorCodes.end())
	{
		str << "Source: Freelancer.exe" << std::endl;
		p = addressErrorCodes[DWORD(GetModuleHandle(nullptr)) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)].c_str();
	}

	else if (addressErrorCodes.find(DWORD(GetModuleHandle("common.dll")) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)) != addressErrorCodes.end())
	{
		str << "Source: common.dll" << std::endl;
		p = addressErrorCodes[DWORD(GetModuleHandle("common.dll")) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)].c_str();
	}

	else if (addressErrorCodes.find(DWORD(GetModuleHandle("server.dll")) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)) != addressErrorCodes.end())
	{
		str << "Source: server.dll" << std::endl;
		p = addressErrorCodes[DWORD(GetModuleHandle("server.dll")) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)].c_str();
	}

	else if (addressErrorCodes.find(DWORD(GetModuleHandle("content.dll")) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)) != addressErrorCodes.end())
	{
		str << "Source: content.dll" << std::endl;
		p = addressErrorCodes[DWORD(GetModuleHandle("content.dll")) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)].c_str();
	}

	else if (addressErrorCodes.find(DWORD(GetModuleHandle("alchemy.dll")) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)) != addressErrorCodes.end())
	{
		str << "Source: alchemy.dll" << std::endl;
		p = addressErrorCodes[DWORD(GetModuleHandle("alchemy.dll")) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)].c_str();
	}

	else if (addressErrorCodes.find(DWORD(GetModuleHandle("ntdll.dll")) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)) != addressErrorCodes.end())
	{
		str << "Source: ntdll.dll" << std::endl;
		p = addressErrorCodes[DWORD(GetModuleHandle("ntdll.dll")) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)].c_str();
	}
	else if (addressErrorCodes.find(DWORD(GetModuleHandle("dalib.dll")) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)) != addressErrorCodes.end())
	{
		str << "Source: dalib.dll" << std::endl;
		p = addressErrorCodes[DWORD(GetModuleHandle("dalib.dll")) + DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)].c_str();
	}

	if (addressErrorCodes.find(DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)) != addressErrorCodes.end() || p)
	{
		caption = "Known error code!";
		if (!p)
		{
			p = addressErrorCodes[DWORD(pExPtrs->ExceptionRecord->ExceptionAddress)].c_str();
		}

		str << "ExpAddress: " << pExPtrs->ExceptionRecord->ExceptionAddress << std::endl << "\nSuspected Reason: " << p << std::endl;
		goto end;
	}
	
	for (const auto& callstackError : callstackErrors)
	{
		if (sw.ss.str().find(callstackError.first) != std::string::npos)
		{
			caption = "Potentially known error code";
			str
			<< "Disclaimer: Errors of this type have changing addresses, and sometimes can share similar callstacks with other errors. This error may not be accurate" << std::endl
			<< "Suspected Reason: " << callstackError.second << std::endl;
			goto end;
		}
	}

	HMODULE hMods[1024];
	HMODULE cur;
	HANDLE hProcess = GetCurrentProcess();
	DWORD cbNeeded;
	char szModName[MAX_PATH];
	MODULEINFO mod;
	
	if (EnumProcessModules(hProcess, hMods, sizeof hMods, &cbNeeded))
	{
		for (auto& i : hMods)
		{
			cur = i;
			if (GetModuleFileNameEx(hProcess, i, szModName, sizeof szModName / sizeof(char)) && GetModuleInformation(hProcess, i, &mod, sizeof(MODULEINFO)))
			{
				if (pExPtrs->ExceptionRecord->ExceptionAddress >= mod.lpBaseOfDll && (byte*)pExPtrs->ExceptionRecord->ExceptionAddress <= (byte*)mod.lpBaseOfDll + mod.SizeOfImage)
					break;
			}
		}
	}
	
	str << "\tUnhandled Exception!\n\t-- Important Information --\n" << std::endl
		<< "\tSource: " << szModName << std::endl
		<< "\tRelExpAddr: 0x" << (byte*)mod.lpBaseOfDll + mod.SizeOfImage - (byte*)pExPtrs->ExceptionRecord->ExceptionAddress << std::endl
		<< "\tExpCode: 0x" << pExPtrs->ExceptionRecord->ExceptionCode << std::endl
		<< "\tExpFlags: " << pExPtrs->ExceptionRecord->ExceptionFlags << std::endl
		<< "\tExpAddress: 0x" << pExPtrs->ExceptionRecord->ExceptionAddress << std::endl
		<< "\tPlease report!" << std::endl;

end:
	logger->write(str.str());
	//FatalAppExit(-1, str.str().c_str());
	
	MessageBox(nullptr, str.str().c_str(), caption, MB_ICONWARNING | MB_OK);
	return EXCEPTION_CONTINUE_SEARCH;
}

static void InitUnhandledExceptionFilter()
{
	TCHAR szModName[_MAX_PATH];
	if (GetModuleFileName(nullptr, szModName, sizeof szModName / sizeof(TCHAR)) != 0)
	{
		_tcscpy_s(s_szExceptionLogFileName, szModName);
		_tcscat_s(s_szExceptionLogFileName, ".exp.log");
	}
	
	if (s_bUnhandledExeptionFilterSet == false)
	{
		SetUnhandledExceptionFilter(CrashHandlerExceptionFilter);
		PreventSetUnhandledExceptionFilter();
		s_bUnhandledExeptionFilterSet = true;
	}
}

void LoadAddressStore()
{
	try {
		std::string url = "https://files.aftermath.space/AftAddrList.toml";
		std::string file = fs::current_path().string() + "\\..\\AftAddrList.toml";

		CURL* curl = curl_easy_init();
		if (curl)
		{
			FILE* fp = fopen(file.c_str(), "wb");
			curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, 0);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
			CURLcode res = curl_easy_perform(curl);
			curl_easy_cleanup(curl);
			fclose(fp);
		}

		auto toml = cpptoml::parse_file(file);
		auto table = toml->get_table("BareAddrList");
		for (const auto& pair : *table)
		{
			DWORD hex;
			std::istringstream ss(pair.first);
			ss >> std::hex >> hex;
			addressErrorCodes[hex] = pair.second->as<std::string>()->get();
		}

		table = toml->get_table("ClassMemberList");
		for (const auto& pair : *table)
			callstackErrors[pair.first] = pair.second->as<std::string>()->get();
	} catch (...) {
		MessageBox(GetActiveWindow(), "Crash hopper was unable to initalise. This is likely an internet issue, or malformation in the remote address table.", "Crash Hopper Disabled", MB_ICONWARNING | MB_OK);
	}
}

void * __cdecl ScriptLoadHook(const char *script)
{
	// After the main menu loads, load our address store. We need to delay this because making web calls inside of a dllmain causes a hang
	LoadAddressStore();
	UnDetour((unsigned char*)_ThornScriptLoad, thornLoadData);
	return _ThornScriptLoad(script);
}

void Patch()
{
	char arr[255];
	GetModuleFileName(nullptr, arr, sizeof arr);
	
	std::string loader = _strcmpi(fs::path(std::string(arr)).filename().generic_u8string().c_str(), "FLServer.exe") != 0 ? "ClientLog-" : "ServerLog-";
	
	// Get common dll
	HMODULE common = GetModuleHandleA("common");

	// Grab address of loading base entries
	_ThornScriptLoad = ScriptLoadPtr(GetProcAddress(common, "?ThornScriptLoad@@YAPAUIScriptEngine@@PBD@Z"));

	// Assign 5 bytes to the placeholder
	thornLoadData = static_cast<unsigned char*>(malloc(5));

	// Detour the function with our own for a late init
	Detour(reinterpret_cast<unsigned char*>(_ThornScriptLoad), ScriptLoadHook, thornLoadData);

	// Craete a new global logger and write a new session to it
	logger = new Logger(fs::current_path().string() + "\\..\\" + loader + "AftCrashLog.txt");
	logger->write("\n\n-------------New Session---------------");
	InitUnhandledExceptionFilter(); // Init the crash hopper for unhandled exceptions
}

BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
	if (fdwReason == DLL_PROCESS_ATTACH)
	{
		Patch();
	}

	return TRUE;
}
