#pragma once

#define WIN32_LEAN_AND_MEAN
#define IMPORT _declspec(dllimport)
#define EXPORT _declspec(dllexport)

#include <string>
#include <map>
#pragma comment(lib, "Advapi32.lib")

typedef unsigned int uint;
typedef unsigned long ulong;
typedef unsigned short ushort;

// Memory Patches
static std::map<unsigned long long, std::string> addressErrorCodes;
static std::map<std::string, std::string> callstackErrors;